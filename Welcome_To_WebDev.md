<h3>Welcome to IIITM-K WebDev Group.</h3>
<p> What is web development ?  Web development is the work involved in developing a web site for the Internet or an intranet. Web development can range from developing a simple single static page of plain text to complex web-based internet applications, electronic businesses, and social network services.
This platform is an opportunity for every student to learn the fundamentals of web development by some hands-on projects. Here we will cover basic web technologies like **HTML, CSS & Frameworks , JavaScript & it Libraries** And introduction to some Back-End technologies like** PHP with Codeigniter, Python Flask , Node.js and  various databases like MySql, Postgresql and MongoDB.
Here we are not planning to concentrate more on UI/UX, rather than that we focus on basic front end , basic back end and RESTful Web services**
.</p>
----------------------------------------------------------------------------------------------------------------------------
Admin will add projects with intention to work with specific technologies , which will also helpful to beginners in WebDev. Also students are welcome to suggest topics and projects. We hope you all make use of this platform either by collaborating or understanding  the code.
<br>
<b>Akhil K K</b>
<i>Admin<br> WebDev IIITM-K (SPC)</i>


Note :
    Some of you may feel that web development is not suitable for your selected  domain, but it’s not true. As a computer science postgraduate student we all must be aware of basic web technologies. You can’t distinct web technologies from your career  , because you will definitely use some of this technologies in your projects or demos for sure .
